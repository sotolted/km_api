**KM-API Installation Guide**
-------------------------
-------------------------
Author: Ted Hsu  
Release Date: 2015/04/24

-------------------------
**Package installation**
-------------------------
-------------------------
**1. Python(2.7.6):**

sudo apt-get install build-essential libssl-dev libffi-dev python-dev

sudo apt-get install python-pip
	
**2. Django(1.7.7):**

sudo pip install django

python -c "import django;print(django.get_version())"

**3. Django rest framework:**

sudo pip install djangorestframework

**4. Production server with apache:**

sudo apt-get install apache2

sudo apt-get install libapache2-mod-wsgi

sudo mv path_to/km_api /var/www

**5. Set virtualHost**

sudo vim /etc/apache2/sites-available/000-default.conf

	DocumentRoot /var/www/km_api 
	WSGIScriptAlias / "/var/www/km_api/km_api.wsgi" 
	WSGIPassAuthorization On
	<Directory "/var/www/km_api"> 
	Order deny,allow 
	Allow from all 
	Require all granted 
	<Directory>

**6. Set current user 'foxconn' to run apache2**

sudo vim /etc/apache2/envvars

	export APACHE_RUN_USER=foxconn 
	export APACHE_RUN_GROUP=foxconn


**7. Set ServerName**

sudo vim /etc/apache2/apache2.conf

	ServerName api-server


**8. Set km_api allowed hosts**

sudo vim /var/www/km_api/km_api/settings_production.py

	ALLOWED_HOSTS = ['api-server']


**9. Set Server hosts file**

sudo vim /etc/hosts

	192.168.56.102 elasticsearch api-server

**10. Restart Apache Server**

sudo service apache2 restart

-------------------------
**API list:**
-------------------------
-------------------------

**1. list api**

	curl -X POST http://api-server/auth/

**2. register**

	curl -X POST http://api-server/auth/register/ -H "Content-Type: application/json" -d '{"username":"<yourusername>","password":"<yourpassword>"}'
	     

**3. login**

    curl -X POST http://api-server/auth/login/ -H "Content-Type: application/json" -d '{"username":"<yourusername>","password":"<yourpassword>"}' 

**4. change password**

    curl -X POST http://api-server/auth/password/ -H 'Authorization: Token <your_token>' -H "Content-Type: application/json" -d '{"new_password":"<your_new_password>","re_new_password":"<your_new_password>","current_password":"<your_old_password>"}' 

**5. logout**

    curl -X POST http://api-server/auth/logout/ -H 'Authorization: Token <your_token>' 

**6. check token**

    curl -X POST http://api-server/auth/token/ -H 'Authorization: Token <your_token>' 

-------------------------
**API return format:**
-------------------------
-------------------------
**Http status code:**

	200 ok

**Return string:**

	Success --> {
                 "api_status_code":0,
                 "api_data":{return data} or {}
                }

	Error   --> {
                 "api_status_code":-1,
                 "api_data":{error data}
                }

