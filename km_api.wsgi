import os,sys
os.environ['DJANGO_SETTINGS_MODULE']='km_api.settings_production'
current_dir=os.path.dirname(__file__)
if current_dir not in sys.path:
	sys.path.append(current_dir)
 
from django.core.wsgi import get_wsgi_application
application=get_wsgi_application()