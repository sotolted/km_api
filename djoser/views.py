from django.contrib.auth import get_user_model, logout
from rest_framework import generics, permissions, status, response
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework.reverse import reverse
from django.contrib.auth.tokens import default_token_generator

import datetime
from django.utils.timezone import utc

from . import serializers, settings, utils
from rest_framework.views import APIView

User = get_user_model()

# API: auth/
# Purpose: Show api list
# Method: POST
# Header: 
# Parameter:
class RootView(utils.ActionViewWithoutSerializerMixin, APIView):
    """
    Root endpoint - use one of sub endpoints.
    """

    def action(self, request):
        urls_mapping = {
            'register': 'register',
            'login': 'login',
            'logout': 'logout',
            'change-password': 'set_password',
            'check-token': 'check_token',
        }
        data = dict([(key, reverse(url_name, request=request, format=None))
                    for key, url_name in urls_mapping.items()])
        return data


# API: auth/register/
# Purpose: Register a user
# Method: POST
# Header: Content-Type: application/json
# Parameter: username,password
class RegistrationView(utils.ActionViewMixin, generics.GenericAPIView):
    """
    Use this endpoint to register new user.
    """
    serializer_class = serializers.UserRegistrationSerializer
    permission_classes = (
        permissions.AllowAny,
    )

    def action(self, request, serializer):
        instance = serializer.save()
        data = {}
        return data


# API: auth/login/
# Purpose: User login
# Method: POST
# Header: Content-Type: application/json
# Parameter: username,password
class LoginView(utils.ActionViewMixin, generics.GenericAPIView):
    """
    Use this endpoint to obtain user authentication token.
    """
    serializer_class = serializers.UserLoginSerializer
    permission_classes = (
        permissions.AllowAny,
    )

    def action(self, request, serializer):
        token, created = Token.objects.get_or_create(user=serializer.object)
        
        # process expired token and single user login
        if not created:
            token.delete()
            token = Token.objects.create(user=serializer.object)
            
        token.created = datetime.datetime.utcnow().replace(tzinfo=utc)
        token.save()
        data = serializers.TokenSerializer(token).data        
        return data


# API: auth/logout/
# Purpose: User logout
# Method: POST
# Header: Authorization: Token token_string
# Parameter: 
class LogoutView(utils.ActionViewWithoutSerializerMixin, APIView):
    """
    Use this endpoint to logout user (remove user authentication token).
    """
    permission_classes = (
        permissions.IsAuthenticated,
    )

    def action(self, request):
        Token.objects.filter(user=request.user).delete()
        logout(request)
        data = {}
        return data


# API: auth/password/
# Purpose: Reset user password
# Method: POST
# Header: Authorization: Token token_string
#         Content-Type: application/json        
# Parameter: new_password,re_new_password,current_password
class SetPasswordView(utils.ActionViewMixin, generics.GenericAPIView):
    """
    Use this endpoint to change user password.
    """
    serializer_class = serializers.SetPasswordRetypeSerializer
    permission_classes = (
        permissions.IsAuthenticated,
    )

    def action(self, request, serializer):
        self.request.user.set_password(serializer.data['new_password'])
        self.request.user.save()
        data = {}
        return data



# API: auth/token/
# Purpose: Check user token
# Method: POST
# Header: Authorization: Token token_string
# Parameter: 
class CheckTokenView(utils.ActionViewWithoutSerializerMixin, APIView):
    """
    Use this endpoint to logout user (remove user authentication token).
    """
    permission_classes = (
        permissions.IsAuthenticated,
    )

    def action(self, request):
        # If no auth error is created , server will return 200 ok with null data
        data = {}
        return data








# class PasswordResetView(utils.ActionViewMixin, utils.SendEmailViewMixin, generics.GenericAPIView):
#     """
#     Use this endpoint to send email to user with password reset link.
#     """
#     serializer_class = serializers.PasswordResetSerializer
#     permission_classes = (
#         permissions.AllowAny,
#     )
#     token_generator = default_token_generator

#     def action(self, serializer):
#         for user in self.get_users(serializer.data['email']):
#             self.send_email(**self.get_send_email_kwargs(user))
#         return response.Response(status=status.HTTP_200_OK)

#     def get_users(self, email):
#         active_users = User._default_manager.filter(
#             email__iexact=email,
#             is_active=True,
#         )
#         return (u for u in active_users if u.has_usable_password())

#     def get_send_email_extras(self):
#         return {
#             'subject_template_name': 'password_reset_email_subject.txt',
#             'plain_body_template_name': 'password_reset_email_body.txt',
#         }

#     def get_email_context(self, user):
#         context = super(PasswordResetView, self).get_email_context(user)
#         context['url'] = settings.get('PASSWORD_RESET_CONFIRM_URL').format(**context)
#         return context


# class PasswordResetConfirmView(utils.ActionViewMixin, generics.GenericAPIView):
#     """
#     Use this endpoint to finish reset password process.
#     """
#     permission_classes = (
#         permissions.AllowAny,
#     )
#     token_generator = default_token_generator

#     def get_serializer_class(self):
#         if settings.get('PASSWORD_RESET_CONFIRM_RETYPE'):
#             return serializers.PasswordResetConfirmRetypeSerializer
#         return serializers.PasswordResetConfirmSerializer

#     def action(self, serializer):
#         serializer.user.set_password(serializer.data['new_password'])
#         serializer.user.save()
#         return response.Response(status=status.HTTP_200_OK)


# class ActivationView(utils.ActionViewMixin, generics.GenericAPIView):
#     """
#     Use this endpoint to activate user account.
#     """
#     serializer_class = serializers.UidAndTokenSerializer
#     permission_classes = (
#         permissions.AllowAny,
#     )
#     token_generator = default_token_generator

#     def action(self, serializer):
#         serializer.user.is_active = True
#         serializer.user.save()
#         if settings.get('LOGIN_AFTER_ACTIVATION'):
#             token, _ = Token.objects.get_or_create(user=serializer.user)
#             data = serializers.TokenSerializer(token).data
#         else:
#             data = {}
#         return Response(data=data, status=status.HTTP_200_OK)


# class SetUsernameView(utils.ActionViewMixin, generics.GenericAPIView):
#     """
#     Use this endpoint to change user username.
#     """
#     serializer_class = serializers.SetUsernameSerializer
#     permission_classes = (
#         permissions.IsAuthenticated,
#     )

#     def get_serializer_class(self):
#         if settings.get('SET_USERNAME_RETYPE'):
#             return serializers.SetUsernameRetypeSerializer
#         return serializers.SetUsernameSerializer

#     def action(self, serializer):
#         setattr(self.request.user, User.USERNAME_FIELD, serializer.data['new_' + User.USERNAME_FIELD])
#         self.request.user.save()
#         return response.Response(status=status.HTTP_200_OK)


# class UserView(generics.RetrieveUpdateAPIView):
#     """
#     Use this endpoint to retrieve/update user.
#     """
#     model = User
#     serializer_class = serializers.UserSerializer
#     permission_classes = (
#         permissions.IsAuthenticated,
#     )

#     def get_object(self, *args, **kwargs):
#         return self.request.user

