from rest_framework.views import exception_handler
from . import utils
import collections

def convert(data):
    if isinstance(data, basestring):
        return str(data)
    elif isinstance(data, collections.Mapping):
        return dict(map(convert, data.iteritems()))
    elif isinstance(data, collections.Iterable):
        return type(data)(map(convert, data))
    else:
        return data

def custom_exception_handler(exc, context):
    # Call REST framework's default exception handler first,
    # to get the standard error response.
    api_status_code = 0
    api_data = {}
    
    handle_response = exception_handler(exc, context)    
    # Create custom error respond
    if handle_response is not None:  
        api_status_code = -1
        api_data = convert(handle_response.data)
        return utils.create_response(api_status_code, api_data)

    #return handle_response




