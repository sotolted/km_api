from rest_framework.authentication import TokenAuthentication
from rest_framework import exceptions

from . import settings

import datetime
from django.utils.timezone import utc

class ExpiringTokenAuthentication(TokenAuthentication):

    def authenticate_credentials(self, key):
        try:
            token = self.model.objects.select_related('user').get(key=key)
        except self.model.DoesNotExist:
            raise exceptions.AuthenticationFailed('Invalid token')

        if not token.user.is_active:
            raise exceptions.AuthenticationFailed('User inactive or deleted')

        expire_hours = settings.get('TOKEN_EXPIRE_HOURS')
        utc_expired = datetime.datetime.utcnow() - datetime.timedelta(hours=expire_hours)
        
        if token.created < utc_expired.replace(tzinfo=utc) :
            raise exceptions.AuthenticationFailed('Token has expired')

        return (token.user, token)
